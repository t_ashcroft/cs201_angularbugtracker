function Buggy($scope) {
  $scope.todos = [];
  $scope.statuses = ['Open', 'Closed', 'Hopeless...'];
  $scope.markAll = false;
	$scope.selectedItem;
	
  $scope.addTodo = function() {
      if(event.keyCode == 13 && $scope.title){
          $scope.todos.push(
            {
              title:$scope.title,
              desc:$scope.desc,
              status:$scope.selectedItem,
              done:false
            }
          );
          console.log($scope.selectedItem);
          $scope.title = '';
          $scope.desc = '';
      }
  };
  $scope.isTodo = function(){
      return $scope.todos.length > 0;
  }
  $scope.toggleEditMode = function(){
      $(event.target).closest('li').toggleClass('editing');
  };
  $scope.editOnEnter = function(todo){
      if(event.keyCode == 13 && todo.title){
          $scope.toggleEditMode();
      }
  };

  $scope.remaining = function() {
    var count = 0;
    angular.forEach($scope.todos, function(todo) {
      count += todo.done ? 0 : 1;
    });
    return count;
  };

  $scope.hasDone = function() {
      return ($scope.todos.length != $scope.remaining());
  }

  $scope.itemText = function() {
      return ($scope.todos.length - $scope.remaining() > 1) ? "bugs" : "bug";
  };

  $scope.toggleMarkAll = function() {
      angular.forEach($scope.todos, function(todo) {
        todo.done =$scope.markAll;
      });
  };

  $scope.clear = function() {
    var oldTodos = $scope.todos;
    $scope.todos = [];
    angular.forEach(oldTodos, function(todo) {
      if (!todo.done) $scope.todos.push(todo);
    });
  };

 // $scope.completed = function() {
//	angular.forEach($scope.todos, function(todo){
//		if(todo.selectedItem != "Closed") todo.selectedItem = "Closed";
//	});  
 // };
  
  $scope.statusSelected = function(status) {
    $scope.selectedItem = status;
    console.log($scope.selectedItem);
  };

}
